<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index(Request $request){
//        $keyword = request('keyword');
        if(!is_null($request)){
            $products = Product::where('name', 'Like', "%{$request->name}%")
                ->orWhere('price', 'Like', "%{$request->price}%")
                ->orWhere('created_at', 'Like', "%{$request->created_at}%")
                ->orWhere('updated_at', 'Like', "%{$request->updated_at}%")
                ->paginate(5);
        }else{
            $products = Product::paginate(5);
        }

//       $products=Product::Paginate(5);
       return view('welcome',compact('products'));
    }

}
