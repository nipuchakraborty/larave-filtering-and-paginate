<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\Product;
class ProductSeeder extends Seeder
{

    public function run()
    {
//        DB::table('products')->insert([
//            'name' => 'cosmatices',
//            'price' => 2791,
//            'created_at' => date("Y-m-d H:i:s"),
//            'updated_at' => date("Y-m-d H:i:s")
//        ]);


        $faker = Faker\Factory::create();

        for($i = 0; $i < 1000; $i++) {
            Product::create([
                'name' => $faker->name,
                'price' => $faker->randomDigit(598),
                'created_at' =>$faker-> date("Y-m-d H:i:s"),
                'updated_at' => $faker->date("Y-m-d H:i:s")
            ]);
        }
    }
}
