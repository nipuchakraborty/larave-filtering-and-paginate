<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Welcome</title>
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>

<form action="{{route('home')}}">
<input type="text" name="name">
<input type="number" name="price">
<input type="date" name="created_at">
<input type="date" name="updated_at">
    <input type="submit">
</form>
    <br/>
    <br/>
<table class="table table-bordered">
  <thead>
  <tr>
      <th>ID</th>
      <th>Product Name</th>
      <th>Product Price</th>
      <th>created at</th>
      <th>updated at</th>
      <th>Action</th>
  </tr>
  </thead>
    <tbody>
   @foreach($products as $product)
       <tr>
           <td>{{$product->id}}</td>
           <td>{{$product->name}}</td>
           <td>{{$product->price}}</td>
           <td>{{$product->created_at}}</td>
           <td>{{$product->updated_at}}</td>
           <td>
               <button>Edit</button>
               <button>Del</button>
           </td>
       </tr>
       @endforeach
    </tbody>
</table>
{{ $products->links()}}

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>
